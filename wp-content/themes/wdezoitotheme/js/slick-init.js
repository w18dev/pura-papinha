jQuery(document).ready(function($){
	$('.slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		fade: true,
		infinite: true,
		adaptiveHeight: true,
		autoplay: true,
		autoplaySpeed: 2000,
		dots: true
	});
});