function initialize(){
        $('.map').each(function (index, Element) {
                var coords = $(Element).text().split(",");
        if (coords.length != 4) {
            $(this).display = "none";
            return;
        }
        if(coords[3]==="1"){
            var icone = '../wp-content/themes/wdezoitotheme/images/marker-loja-propria.png';
        }else{
            var icone = '../wp-content/themes/wdezoitotheme/images/marker-ponto-venda.png';
        }

        if (typeof google === 'undefined') {

        }
        var latlng = new google.maps.LatLng(parseFloat(coords[0]), parseFloat(coords[1]));
        var myOptions = {
            zoom: parseFloat(coords[2]),
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: false,
            mapTypeControl: true,
            zoomControl: true,
            scrollwheel: false,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL
            }
        };
        var map = new google.maps.Map(Element, myOptions);
     
        var marker = new google.maps.Marker({
            position: latlng,
            center: latlng,
            icon: icone,
            map: map
        });
        map.addListener('center_changed', function() {
            window.setTimeout(function() {
                map.panTo(marker.getPosition());
            }, 500);
        });

        marker.addListener('click', function() {
            map.setZoom(8);
            map.setCenter(marker.getPosition());
        });

    });
}

jQuery(document).ready(function($) {
	$('.btnSearch').click(function(data){
		$("#formPesquisa").submit();
	});
	$(".scroll").click(function(event){    
		event.preventDefault();
		$('html,body').animate({scrollTop:$(this.hash).offset().top-200}, 1500);
	});
	
  if(window.location.hash==="#contato"){
    $('html,body').animate({scrollTop:$("#contato").offset().top-200}, 1500);
  }



    /* MENU */
    $('.navbar-header').click(function(event) {
    	$('.menuListMobile').toggle('slow');
    });

});