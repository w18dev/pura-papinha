
<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/slick.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/slick-theme.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/jquery.fancybox.min.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/champs.min.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/estilo.css">
    <!--[if lt IE 8]>
        <script type="text/javascript" src="js/html5shiv.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
    <script type="text/javascript">
        window.onload = function() {
            document.getElementById('pageFull').style.opacity = '1';
            document.getElementById('loading').style.display = 'none';
        }
    </script>

</head>
<body <?php echo @$singlePost; ?>>
<!-- <img id="loading" src="<?php //bloginfo('template_directory'); ?>/images/loading.gif" /> -->
<div id="loading">
    <img class="loadingImg" src="<?php bloginfo('template_directory'); ?>/images/loading.gif" />
    <br/>Carregando
</div>
<style type="text/css">
    #loading {
        position: fixed;
        width: 100%;
        height: 100%;
        background: #fff;
        z-index: 9999;
        text-align: center;
        opacity: 0.9;
        padding-top: 250px;
        color: #4d92b1;
        font-size: 25px;
        font-family: 'GothamRoundedBold';
    }

</style>
<div id="pageFull">
    <header>
        <div class="header-list row container center">
            <div class="lato-bold text-center menuMobile">
                <div class="row">
                    <div class="hidden-lg hidden-md col-sm-1 col-xs-1">
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                        </div>
                    </div>
                    <div class="hidden-lg hidden-md col-sm-5 col-xs-3">
                        <a class="logoMobile" href="<?php bloginfo('url'); ?>/index.php"><img src="<?php bloginfo('template_directory'); ?>/images/logo-header.png" /></a></li>
                    </div>
                    <div class="hidden-lg hidden-md col-sm-6 hidden-xs" style="float: right;">
                        <button class="botao botao-carrinho"><img src="<?php bloginfo('template_directory'); ?>/images/icon-carrinho.png" /> PEÇA SUA PAPINHA </button>
                    </div>
                    <div class="hidden-lg hidden-md hidden-sm col-xs-8" style="float: right;">
                        <button class="botao botao-carrinho"><img src="<?php bloginfo('template_directory'); ?>/images/icon-carrinho.png" /> </button>
                    </div>
                </div>
                <div class="row">
                    <div class="menuListMobile hidden-lg hidden-md col-sm-12 col-xs-12">
                        <ul>
                            <?php
                                $id = get_the_ID();
                                $linkContato = ($id==77) ? "#contato" : "/pura-papinha/#contato";
                                $linkClass = ($id==77) ? "scroll" : "";
                            ?>
                            <li><a href="/pura-papinha/quem-somos">SOBRE NÓS</a></li>
                            <li><a href="/pura-papinha/nossas-papinhas">NOSSAS PAPINHAS</a></li>
                            <li><a href="/pura-papinha/dicas-da-nutri">DICAS DA NUTRI</a></li>
                            <li><a href="/pura-papinha/duvidas">DÚVIDAS</a></li>
                            <li><a class="<?php echo $linkClass; ?>" href="<?php echo $linkContato; ?>">CONTATO</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <nav class="lato-bold text-center menuDesktop">
                <ul>
                    <?php
                        $id = get_the_ID();
                        $linkContato = ($id==77) ? "#contato" : "/pura-papinha/#contato";
                        $linkClass = ($id==77) ? "scroll" : "";
                    ?>
                    <li><a href="/pura-papinha/quem-somos">SOBRE NÓS</a></li>
                    <li><a href="/pura-papinha/nossas-papinhas">NOSSAS PAPINHAS</a></li>
                    <li><a href="/pura-papinha/dicas-da-nutri">DICAS DA NUTRI</a></li>
                    <li><a href="<?php bloginfo('url'); ?>/index.php"><img src="<?php bloginfo('template_directory'); ?>/images/logo-header.png" /></a></li>
                    <li><a href="/pura-papinha/duvidas">DÚVIDAS</a></li>
                    <li><a class="<?php echo $linkClass; ?>" href="<?php echo $linkContato; ?>">CONTATO</a></li>
                    <button class="botao botao-carrinho"><img src="<?php bloginfo('template_directory'); ?>/images/icon-carrinho.png" /> PEÇA SUA PAPINHA </button>
                </ul>
            </nav>

        </div>
    </header> 