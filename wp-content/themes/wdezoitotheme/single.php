<?php $singlePost = 'id="singlePost"'; ?>
<?php include 'header.php'; ?>
<session id="banner-dicasnutri">
    <div class="">
        <div class="row">              
            <div class="col-md-12 col-xs-12">
            <?php $image = get_field_object( "field_58cc486b2cc27", 48 ); ?>
            <img style="width: 100%; height: 0 auto;" src="<?php echo $image['value']['url']; ?>" />
            </div>
        </div>
    </div>
</session>
<section id="post-single">
	<div class="container center">
    <div class="col-md-12 col-xs-12">
        <?php
             while (have_posts()) {
                 the_post();
                 $postID = get_the_ID();
                 $categoria = get_the_category($postID);
                 $data = get_the_date( $format, $postID );
                 $autor = get_the_author();
                 $posttags = get_the_tags();
                 $categoria = get_the_category( $postID ); 
                 $conteudo = get_the_content();
             }
           
             ?>
        <div class="tituloPost"><?php the_title() ?></div>
        <hr class="barraPost" />
        <p class="infoTexto">por <?php echo the_author_meta( 'display_name' , $v->post_author ); ?> / <?php echo $categoria[0]->name; ?> / <?php echo get_the_date( $format, $postID )?></p>
        <?php $imagem = get_field_object( "field_58cc4bd12651f", $postID ); ?>
        <div class="imagePost">
           <img class="listaPost-imagePost" src="<?php echo $imagem['value']['url']; ?>" />
        </div>
        <div class="PostText"><?php the_field( "field_58daaab52af3c", $postID); ?></div>
     </div>
      <div class="social">
        <?php if ( dynamic_sidebar('social_widgets') ) : else : endif; ?>
      </div>
   </div>
</section>

<section id="comentarios-single">
  <div class="container center">
    <h4>COMENTÁRIOS</h4>
    <?php comment_form(); ?>
    <?php
        $comment_array = get_approved_comments($postID); 
        if ($comment_array) {
            foreach($comment_array as $comment){
              echo '<div class="col-md-12 col-xs-12">';
              echo "<hr/>";
              echo "<div class='infoauthor'>";
              echo '<span class="comment_author"> '.$comment->comment_author.' </span>';
              echo '<span class="comment_date"> - '.date('d/m/Y (H\hi)', strtotime($comment->comment_date)).' </span><br/>';
              echo '</div>';
              echo '<span class="comment_content"> '.$comment->comment_content.' </span>';
              echo '</div>';
            }
        }
     ?>
 </div>
</section>
<?php include 'footer.php'; ?>
