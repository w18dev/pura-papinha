<?php include 'header.php'; ?>
   

<?php 
  $images = get_field_object('field_58c9379eb6632', 'option');
  if( $images ): ?>
    <session id="banner-home">
      <div class="">
        <div class="row">              
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
            <div class="slider-for">
              <?php foreach( $images['value'] as $image ): ?>
                <div class="banner bannerTopo"  style="backgroung-position: center; background-color: #fffef0; height:<?php echo $image['height']; ?>px; width: 100%; background-image: url('<?php echo $image['url']; ?>');"></div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
    </session>
<?php endif; ?>
  <!--<session id="banner-home">
      <div class="">
          <div class="row">              
              <div class="col-md-12 col-xs-12">
                  <div class="banner" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/banner-topo.jpg)"></div>
              </div>
          </div>
      </div>
    </session> -->
    <session id="primeira-sessao">                
        <img src="<?php bloginfo('template_directory'); ?>/images/fruta-fundo-topo-4.png" class="fotofundo fototopo4"/>
        <img src="<?php bloginfo('template_directory'); ?>/images/fruta-fundo-topo-1.png" class="fotofundo fototopo1"/>
        <img src="<?php bloginfo('template_directory'); ?>/images/fruta-fundo-topo-2.png" class="fotofundo fototopo2"/>
        <img src="<?php bloginfo('template_directory'); ?>/images/fruta-fundo-topo-3.png" class="fotofundo fototopo3"/>

        <div class="row center pageQuemSomos">              
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 center">
                <div class="row center">
                	<div class="col-lg-6 col-md-6 col-md-6 col-sm-12 col-xs-12 quem-somos">
                		<h1 class="titulo"><?php the_field( "field_58c8592b5de76", 'option'); ?></h1>
                		<span>
                		  <?php the_field( "field_58c859a297863", 'option'); ?> 
                		</span>
                		<a href="<?php the_field( "field_58c859c297865", 'option'); ?>" class="botao botao-saibamais">SAIBA MAIS</a>
                	</div>
		      		    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 box-video-home">
                    <a data-fancybox href="https://www.youtube.com/embed/0WHSYgq9Vz0">
                      <img src="<?php bloginfo('template_directory'); ?>/images/video-home.png" style="width: 100%" />








                    </a>
                  </div>
                </div>
            </div>
        </div> 
        <div class="row separador-frutas">  
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 center">
                <div class="row">  
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                        <img src="<?php bloginfo('template_directory'); ?>/images/separador-frutas.png" width="100%" />
                    </div>
                </div>
            </div>
        </div>   
        <div class="row pageNossasPapinhas">              
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 center">
                <div class="row">
                    <div class="col-lg-8 col-md-12 nossas-papinhas">
                        <h1 class="titulo"><?php the_field( "field_58c859e497866", 'option'); ?></h1>                    
                        <span><?php the_field( "field_58c85a0397867", 'option'); ?></span>
                    </div>                    
                     <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nossas-papinhas-lista">
                           <ul>
                               <li>
                                   <a href="#">
                                       <span>Frutinha</span>
                                       <img src="<?php bloginfo('template_directory'); ?>/images/frutinha-home.png" />
                                   </a>
                               </li>
                               <li>
                                   <a href="#">
                                       <span>Papinha 6+</span>
                                       <img src="<?php bloginfo('template_directory'); ?>/images/papinha6-home.png" />
                                   </a>
                               </li>
                               <li>
                                   <a href="#">
                                       <span>Papinha 8+</span>
                                       <img src="<?php bloginfo('template_directory'); ?>/images/papinha6-home.png" />
                                   </a>
                               </li>                                                              
                               <li>
                                   <a href="#">
                                       <span>Comidinhas 12+</span>
                                       <img src="<?php bloginfo('template_directory'); ?>/images/comidinha12-home.png" />
                                   </a>
                               </li>                               
                           </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 botao-conhecatodas" >
                          <a href="<?php the_field( "field_58c930e5f47f9", 'option'); ?>" class="botao botao-conheca">CONHEÇA TODAS</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>     
    </session>
    <session id="segunda-sessao">  
        <div class="fotofundo fotovisite2" ></div>
        <div class="fotofundo fotovisite1" ></div>              

        <div class="divisor divisor-rosa" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/divisor-rosa.png)"></div>
        <div class="row pageNossaLojinha">              
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 center">
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 visite-nossa-lojinha">
                        <h1 class="titulo"><?php the_field( "field_58c85a1497868", 'option'); ?></h1>
                        <span>
                            <?php the_field( "field_58c85a2b97869", 'option'); ?>
                        </span>
                        <a href="<?php the_field( "field_58c85a2b97869", 'option'); ?>" class="botao botao-carrinho"><img src="<?php bloginfo('template_directory'); ?>/images/icon-carrinho.png" /> PEÇA SUA PAPINHA </a>
                    </div>
                    <div class="col-lg-7 col-md-7 box-nuvem-fotos">
                        <img src="<?php bloginfo('template_directory'); ?>/images/imagem-nuvem-fotos.png" />              
                    </div>
                </div>
            </div>
        </div> 
    </session>
    <session id="contato">                
        <img src="<?php bloginfo('template_directory'); ?>/images/fruta-fundo-contato-1.png" class="fotofundo fotocontato1"/>
        <img src="<?php bloginfo('template_directory'); ?>/images/fruta-fundo-contato-2.png" class="fotofundo fotocontato2"/>              

        <div class="divisor divisor-azul" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/divisor-azul.png)"></div>
        <div class="row linhacontato">              
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 center">
                <div class="row pageContato">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 contato">
                        <h1 class="titulo">CONTATO</h1>
                        <span>
                            <form method="POST" class="formContato">
                                <div class="row">              
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        
                                            <div class="campoInfo col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <input class="inputContato" type="text" name="nomecompleto" placeholder="Nome Completo" />
                                            </div>
                                        
                                        
                                            <div class="campoInfo col-lg-6 col-md-6 col-sm-12 col-xs-12 colunaform">
                                                <input class="inputContato" type="email" name="email" placeholder="E-mail" />
                                            </div>
                                            <div class="campoInfo col-lg-6 col-md-6 col-sm-12 col-xs-12 colunaform">
                                                <input class="inputContato" type="phone" name="telefone" placeholder="Telefone" />
                                            </div>
                                        
                                        
                                            <div class="campoInfo col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <input class="inputContato" type="text" name="assunto" placeholder="Assunto" />
                                            </div>
                                        
                                        
                                            <div class="campoInfo col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <textarea class="inputContato" placeholder="Mensagem"></textarea>
                                            </div>
                                        
                                    </div>
                                    <div class="row">
                                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <button class="botao botao-conheca" style="float: right;">ENVIAR</button>
                                      </div>
                                    </div>
                                </div>                                
                            </form>
                        </span>                        
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 mapa-contato">
                        <div class="box-mapa-contato">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d29568.641008715178!2d-51.423811949999994!3d-22.1229205!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spt-BR!2sbr!4v1485866572519" width="100%" height="305" frameborder="0" style="border:0" allowfullscreen></iframe>
                            <span>Presidente Prudente - SP</span>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </session>

<?php include 'footer.php'; ?>