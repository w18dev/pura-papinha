<?php include 'header.php'; ?>
<session id="banner-dicasnutri">
    <div class="">
        <div class="row">              
            <div class="col-md-12 col-xs-12">
        		<?php $image = get_field_object( "field_58cc486b2cc27", 48 ); ?>
        		<img style="width: 100%; height: 0 auto;" src="<?php echo $image['value']['url']; ?>" />
            </div>
        </div>
    </div>
</session>
<session id="dicas-nutri">
    <div class="container center">          
        <div class="row">          
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id="tituloDicasNutri">
                    <img class="center" src="<?php bloginfo('template_directory'); ?>/images/dicas-nutri-titulo.png" />
                </div>
            </div>
        </div>
    </div>
	<div class="container center">          
    	<div class="row">          
            <?php 
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                $args = array(
                    'prev_next'          => false,
                    'prev_text'          => '<',
                    'next_text'          => '>',
                    'post_status'        => 'publish',
                    'paged' => $paged,
                );

                $posts = query_posts($args);
                while(have_posts()):
                    the_post();
                    $postId = $post->ID;
                    $titulo = $post->post_title;
                    $categoria = get_the_category( $postId ); 
                    $conteudo = get_the_content();
            ?>
                <div class="boxPost">
                    <?php $imagem = get_field_object( "field_58cc4bd12651f", $postId ); ?>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 left" >
                        <div class="imagemPostDicasNutri" style="background-image: url('<?php echo $imagem['value']['url']; ?>')" >
                        </div>
                    </div>
                    <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-12 left rowTextoDicasNutri" >
                        <p class="infoTexto">por <?php echo the_author_meta( 'display_name' , $v->post_author ); ?> / <?php echo $categoria[0]->name; ?> / <?php echo get_the_date( $format, $postId )?></p>
                        <p class="postTitulo"><?php echo $titulo; ?></p>
                        <div class="PostText"><?php echo substr($conteudo, 0, 320); ?>...</div>
                    </div>
                    <a class="btnLeiaMais" href="<?php the_permalink(); ?>" > LEIA MAIS </a>
                </div>
            <?php endwhile; ?>   
            <div class="paginacao">
                <div class="numeros">
                <?php if(function_exists('wp_corenavi')) { wp_corenavi(); } ?>
                </div>   
                <div class="pageTotal">
                    <?php
                        echo "página ";
                        echo $paged;
                        echo " de ";
                        global $wp_query;
                        echo $wp_query->max_num_pages;
                    ?>
                </div>   
            </div>   

        </div>
    </div>

</session>
<?php include 'footer.php'; ?>