var CidadeEstado = function(){
  this.jsonPath = "/mae-coruja/wp-content/themes/wdezoitotheme/js/estados-cidades.json"
}
CidadeEstado.prototype.listarEstados = function(config){
  var path = this.jsonPath;
  $.ajax({
    dataType: 'json',
    type: 'post',
    url: path,
    success: function(results){
      var result = results.estados,
          selector = document.getElementById(config.selector),
          rows = Object.keys(result).length;
      for(var i = 0; i < rows; i++){
          var item = result[i], 
          option = document.createElement("option");
          option.setAttribute("value", item.sigla);
          option.innerHTML =  item.nome;
          selector.appendChild(option);
        }
      }
  });
}

CidadeEstado.prototype.listarCidades = function(config){
    var path = this.jsonPath;
    $.ajax({
      dataType: 'json',
      type: 'post',
      url: path,
      success: function(results){
        var result = results.estados,
            selector = document.getElementById(config.selector),
            rows = Object.keys(result).length,
            items = selector.children,
            itemsSize = items.length;
        $("#"+config.selector).empty();
        var first = document.createElement("option");
        first.setAttribute("value", "");
        first.innerHTML="Cidade";
        selector.appendChild(first);
        for(var i = 0; i < rows; i++){
          var item = result[i]; 
          if(config.estado == item.sigla){
            var cidades = item.cidades;
            for(var e = 0; e < cidades.length; e++){
              var option = document.createElement("option");
              option.setAttribute("value", cidades[e]);
              option.innerHTML = cidades[e];
              selector.appendChild(option); 
            }
          }
        }
      }
    });
}