$(document).ready(function(){
	var carousel = $("#espaco--carousel");
	carousel.owlCarousel({
		items:4,
		responsiveClass:true,
		responsive:{
			320:{
				items:1,
			},
			768:{
				items:2,
			},
			1024:{
				items:4
			}
		}
	});
	$('#button-next').click(function(){
		carousel.trigger('next.owl.carousel');
	});
	$('#button-prev').click(function(){
		carousel.trigger('prev.owl.carousel');
	});
	var cidadeestado = new CidadeEstado();
	cidadeestado.listarEstados({
		selector: "ddlEstado"
	});

	 $("#ddlEstado").on("change", function(){
	 	/*selectClear("ddlCidade");*/
	 	var idEstado = $(this).val();
	 	cidadeestado.listarCidades({
	 		selector: "ddlCidade", 
	 		estado: idEstado
	 	})
	 });

	 $("#Enviar").on("click", function(e){
	 	e.preventDefault();
	 	if(formCheckRequerid()){
	 		$("#Contato").submit();
	 	}
	 })

	$('#open-menu').click(function(){
		$('#close-menu').css({'right' : '0'});
		
	});
	$("#btnClose").on("click", function(){
		$('#close-menu').css({'right' : '-100%'});
	});



});

