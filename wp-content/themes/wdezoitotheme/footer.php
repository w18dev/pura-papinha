    <footer class="rodape">
      <div class="row pageRodape">              
        <div class="col-lg-8 col-md-8 col-sm-12 col-md-12 center">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 divRodapeLogo ">
                <img src="<?php bloginfo('template_directory'); ?>/images/logo-branca-rodape.png" />
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 box-sitemap">
                 <h2>Institucional</h2>
                 <ul>
                   <li><a href="<?php echo get_site_url(); ?>/quem-somos/">Sobre nós</a></li>
                   <li><a href="<?php echo get_site_url(); ?>/nossas-papinhas">Nossas Papinhas</a></li>
                   <li><a href="<?php echo get_site_url(); ?>/dicas-da-nutri">Dica da Nutri</a></li>
                   <li><a href="<?php echo get_site_url(); ?>/duvidas">Dúvidas</a></li>
                   <li><a href="<?php echo get_site_url(); ?>/onde-encontrar">Onde encontrar</a></li>
                   <li><a href="<?php echo get_site_url(); ?>/contato">Contato</a></li>
                 </ul>                 
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 box-sitemap">
                <h2>Loja Online</h2>
                <ul>
                   <li><a href="#">Categorias</a></li>
                   <li><a href="#">Carrinho</a></li>
                   <li><a href="#">Login/Cadastro</a></li>
                   <li><a href="#">Como Comprar</a></li>                   
                 </ul>                 
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 box-sitemap infoContato">
                <h2>Newsletter</h2>
                <ul>
                   <li>
                      <form>
                          <input placeholder="Seu e-mail" type="email" name="email" />
                          <img src="<?php bloginfo('template_directory'); ?>/images/botao-enviarnewsletter.png">
                      </form>
                   </li>                   
                   <li>
                     <img src="<?php bloginfo('template_directory'); ?>/images/icon-telefone.png" />
                     <a href="">18 3355 2000</a>
                   </li>
                   <li>
                     <img src="<?php bloginfo('template_directory'); ?>/images/icon-whatsapp.png" />
                     <a href="">18 9876 5310</a>
                   </li>
                   <li>
                     <img src="<?php bloginfo('template_directory'); ?>/images/icon-email.png" />
                     <a href="">sac@purapapinha.com</a>
                   </li>                   
                   <li>
                      <a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/icon-facebook.png" style="margin-right: 10px; margin-top: 15px;"></a>
                      <a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/icon-instagram.png"></a>
                   </li>
                 </ul>                 
            </div>
        </div>
      </div>
    </footer>
    <section>
      <div class="row">              
        <div class="col-lg-12 col-md-12 center">
            <div class="barra-final" style="height: 50px;">

                  © 2016 TODOS OS DIREITOS RESERVADOS | PURA PAPINHA
                    <p style=" display: block; text-align: right; position: relative; top: -37px;">Desenvolvido por 
                        <a style="position: relative; top: 5px;" href="http://www.wdezoito.com.br/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/logo-w18.png" /></a>
                    </p>


            </div>
        </div>
      </div>      
    </section>		
		
 <script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>

 <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.fancybox.min.js"></script>
 <script src="<?php bloginfo('template_directory'); ?>/js/champs.min.js"></script>
 <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/slick.min.js"></script>
 <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/slick-init.js"></script>
 <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/script.js"></script>
<?php wp_footer(); ?>
  </div>
</body>
</html>