<?php include 'header.php'; ?>
<session id="banner-nossaspapinhas">
    <div class="">
        <div class="row">              
            <div class="col-md-12 col-xs-12">
        		<?php $image = get_field_object( "field_591daeded3a2b", 140 ); ?>
        		<img style="width: 100%; height: 0 auto;" src="<?php echo $image['value']['url']; ?>" />
            </div>
        </div>
    </div>
</session>
<session id="nossas-papinhas">
	<div id="backgroundEsquerda"></div>
	<div id="backgroundDireita"></div>
	<div class="container center">          
    	<div class="row">          
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		    	<div id="tituloNossasPapinhas">
		    		<img class="center" src="<?php bloginfo('template_directory'); ?>/images/nossas-papinhas.png" />
		    	</div>
			</div>
		</div>

		<div class="row bloco1">          
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 submenuCategorias">
		    	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 menuCategoriaFrutinhas active ">
		    		<i class="fa fa-star starFruta" aria-hidden="true"></i> FRUTINHAS
	    		</div>
	    		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 menuCategoriaPapinha6">
		    		<i class="fa fa-star star6" aria-hidden="true"></i> PAPINHA +6
	    		</div>
	    		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 menuCategoriaPapinha8">
		    		<i class="fa fa-star star8" aria-hidden="true"></i> PAPINHA +8
	    		</div>
	    		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 menuCategoriaPapinha12">
		    		<i class="fa fa-star star12" aria-hidden="true"></i> PAPINHA +12
	    		</div>
		    </div>
	    </div>



		<?php

			//$client = new SoapClient('http://projetos.wdezoito.com.br:82/loja-purapapinha/api/v2_soap/?wsdl');
			//$session = $client->login('wdezoito', '281752382135340612661739727520');
//
			//$arrayProdutos = [];
			//$star = "starFruta";
//
			//$produtos = $client->catalogCategoryAssignedProducts($session, 4);
//
			//// percorrendo todos os produtos
			//foreach ($produtos as $kProd => $vProd) :
			//	$produto = $client->catalogProductInfo($session, $vProd->product_id);
			//	$arrayProdutos[$produto->product_id] = $produto;
//
			//	unset($imagem);
//
			//	// buscando imagem
			//	$imagem = $client->catalogProductAttributeMediaList($session, $produto->product_id);
			//	$arrayProdutos[$vProd->product_id]->imagem = $imagem[0]->url;
			//endforeach;

			include("lista-produtos.php");
		?>

    </div>
</session>
<?php include 'footer.php'; ?>
