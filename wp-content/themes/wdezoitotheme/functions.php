<?php
/**
 * Criando uma area de widgets
 */
function widgets_novos_widgets_init() {

	register_sidebar( array(
		'name' => 'social',
		'id' => 'social_widgets',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	) );

	register_sidebar( array(
		'name' => 'faq',
		'id' => 'faq_widgets',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	) );


}
add_action( 'widgets_init', 'widgets_novos_widgets_init' );

add_action('init', 'type_post_servicos');
add_action('init', 'type_post_servicos_list');
add_action('init', 'type_post_galeria');

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		"page_title" => 'Pagina Inicial'
	));
	
}

function wp_corenavi() {
	global $wp_query, $wp_rewrite;
	$pages = '';
	$max = $wp_query->max_num_pages;
	if (!$current = get_query_var('paged')) $current = 1;
	$a['base'] = str_replace(999999999, '%#%', get_pagenum_link(999999999));
	$a['total'] = $max;
	$a['current'] = $current;
	 
	$total = 1; //1 - display the text "Page N of N", 0 - not display
	$a['mid_size'] = 5; //how many links to show on the left and right of the current
	$a['end_size'] = 1; //how many links to show in the beginning and end
	$a['prev_text'] = '<'; //texto para o link "Página seguinte"
    $a['next_text'] = '>'; //texto para o link "Página anterior"
	 
	if ($max > 1) echo '<div>';
		echo $pages . paginate_links($a);
	if ($max > 1) echo '</div>';
}




	function type_post_servicos() { 
		$labels = array(
			'name' => _x('serviços', 'post type general name'),
			'singular_name' => _x('Serviços', 'post type singular name'),
			'add_new' => _x('Adicionar Novo', 'Novo item'),
			'add_new_item' => __('Novo Item'),
			'edit_item' => __('Editar Item'),
			'new_item' => __('Novo Item'),
			'view_item' => __('Ver Item'),
			'search_items' => __('Procurar Itens'),
			'not_found' =>  __('Nenhum registro encontrado'),
			'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
			'parent_item_colon' => '',
			'menu_name' => 'Serviços'
		);
 
		$args = array(
			'labels' => $labels,
			'public' => true,
			'public_queryable' => true,
			'show_ui' => true,			
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'has_archive' => true,
			'hierarchical' => false,
			'menu_position' => null,
			'register_meta_box_cb' => 'servicos_meta_box',		
			'supports' => array('title','editor','thumbnail','comments', 'excerpt', 'custom-fields', 'revisions', 'trackbacks')
          );
 
			register_post_type( 'servicos' , $args );
			flush_rewrite_rules();
}
 
	function type_post_servicos_list() { 
		$labels = array(
			'name' => _x('Serviços lista', 'post type general name'),
			'singular_name' => _x('Serviços Lista', 'post type singular name'),
			'add_new' => _x('Adicionar Novo', 'Novo item'),
			'add_new_item' => __('Novo Item'),
			'edit_item' => __('Editar Item'),
			'new_item' => __('Novo Item'),
			'view_item' => __('Ver Item'),
			'search_items' => __('Procurar Itens'),
			'not_found' =>  __('Nenhum registro encontrado'),
			'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
			'parent_item_colon' => '',
			'menu_name' => 'Serviços Lista'
		);
 
		$args = array(
			'labels' => $labels,
			'public' => true,
			'public_queryable' => true,
			'show_ui' => true,			
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'has_archive' => true,
			'hierarchical' => false,
			'menu_position' => null,
			'register_meta_box_cb' => 'servicos_list_meta_box',		
			'supports' => array('title','editor','thumbnail','comments', 'excerpt', 'custom-fields', 'revisions', 'trackbacks')
          );
 
			register_post_type( 'servicos_list' , $args );
			flush_rewrite_rules();
}
 
	function type_post_galeria() { 
		$labels = array(
			'name' => _x('Galeria', 'post type general name'),
			'singular_name' => _x('Todas as Imagens', 'post type singular name'),
			'add_new' => _x('Adicionar Nova Imagem', 'Nova Imagem'),
			'add_new_item' => __('Nova Imagem'),
			'edit_item' => __('Editar Item'),
			'new_item' => __('Novo Item'),
			'view_item' => __('Ver Item'),
			'search_items' => __('Procurar Itens'),
			'not_found' =>  __('Nenhum registro encontrado'),
			'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
			'parent_item_colon' => '',
			'menu_name' => _x('Galeria', 'Todas as Imagens')
		);
 
		$args = array(
			'labels' => $labels,
			'public' => true,
			'public_queryable' => true,
			'show_ui' => true,			
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'has_archive' => true,
			'hierarchical' => false,
			'menu_position' => null,
			'register_meta_box_cb' => 'galeria_meta_box',		
			'supports' => array('title','editor','thumbnail','comments', 'excerpt', 'custom-fields', 'revisions', 'trackbacks')
          );
 
			register_post_type( 'galeria' , $args );
			flush_rewrite_rules();
}

// Modify comments header text in comments
add_filter( 'genesis_title_comments', 'child_title_comments');
function child_title_comments() {
    return __(comments_number( '<h3>No Responses</h3>', '<h3>1 Response</h3>', '<h3>% Responses...</h3>' ), 'genesis');
}
 
// Unset URL from comment form
function crunchify_move_comment_form_below( $fields ) { 
    $comment_field = $fields['comment']; 
    unset( $fields['comment'] ); 
    $fields['comment'] = $comment_field; 
    return $fields; 
} 
add_filter( 'comment_form_fields', 'crunchify_move_comment_form_below' ); 
 
// Add placeholder for Name and Email
function modify_comment_form_fields($fields){
    $fields['author'] = '<div class="campos"> <p class="comment-form-author">' . '<input id="author" placeholder="Nome" name="author" type="text" value="' .
				esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />'.
				
				( $req ? '<span class="required">*</span>' : '' )  .
				'</p>';
    $fields['email'] = '<p class="comment-form-email">' . '<input id="email" placeholder="E-mail" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
				'" size="30"' . $aria_req . ' />'  .
				( $req ? '<span class="required">*</span>' : '' ) 
				 .
				'</p>';
	$fields['url'] = '<p class="comment-form-url">' .
			 '<input id="url" name="url" placeholder="Website" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /> ' .
	           '</p></div>';
	
    return $fields;
}

add_filter('comment_form_default_fields','modify_comment_form_fields');

function wpsites_modify_comment_form_text_area($arg) {
    $arg['comment_field'] = '<div class="campos2"> <textarea id="comment" name="comment" cols="45" rows="8" maxlength="65525" aria-required="true" required="required"></textarea></div>';
    return $arg;
}
add_filter('comment_form_defaults', 'wpsites_modify_comment_form_text_area');

function wpsites_change_comment_form_submit_label($arg) {
$arg['label_submit'] = 'Enviar';
$arg['class_submit'] = 'btnEnviarComentario';
     return $arg;
}

add_filter('comment_form_defaults', 'wpsites_change_comment_form_submit_label');
?>