<?php include 'header.php'; ?>
<session id="banner-quemsomos">
    <div class="">
        <div class="row">              
            <div class="col-md-12 col-xs-12">
        		<?php $image = get_field_object( "field_58c7f28953bb2", 4 ); ?>
        		<img style="width: 100%; height: 0 auto;" src="<?php echo $image['value']['url']; ?>" />
            </div>
        </div>
    </div>
</session>
<session id="quem-somos">
	<div id="backgroundEsquerda"></div>
	<div id="backgroundDireita"></div>
	<div class="container center">          
    	<div class="row">          
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		    	<div id="tituloSobreNos">
		    		<img class="center" src="<?php bloginfo('template_directory'); ?>/images/quem-somos-titulo.png" />
		    	</div>
			</div>
		</div>

		<div class="row bloco1">          
		    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		    	<div class="conteudoSobreNos">
		    		<h1><?php the_field( "field_58c7edad628ee" ); ?></h1>
		    		<?php the_field( "field_58c7edd8628ef" ); ?>

		    	</div>
			</div>
			    <?php
				    $images = get_field_object('field_58c7eded628f0');
				    foreach ($images['value'] as $kImg => $vImg) :
				    	echo "<div class='col-lg-3 col-md-3 col-sm-6 col-xs-12 pessoaDiv'><div class='imagemPessoa'><img src='".$vImg['url']."' /></div><div class='infoPessoa'><h3>".$vImg['title']."</h3><span>".$vImg['description']."</span></div></div>";
				    endforeach;
			    ?>
		</div>

		<div class="row bloco2">          
		    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        		<?php $image = get_field_object( "field_58c7ee17628f1", 4 ); ?>
        		<img style="width: 100%; height: 0 auto;" src="<?php echo $image['value']['url']; ?>" />
			</div>
		    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		    	<div id="conteudoSecundario">
		    		<h1><?php the_field( "field_58c7ee2e628f2" ); ?></h1>
		    		<?php the_field( "field_58c7ee37628f3" ); ?>
		    	</div>
			</div>
		</div>

    </div>
	<div class="row" id="rowValores" >
		<div id="blocoValores"></div>          
		<div class="container center">
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	    		<ul class="listaValores">
	    		<?php $count=1; ?>
	    		<?php while ( have_rows('field_58c7ee5b628f4', 4) ) : the_row(); ?>
	    			<li class="li<?php echo $count; ?>">
	    				<div class="div<?php echo $count; ?>">
    						<img src="<?php bloginfo('template_directory'); ?>/images/quem-somos-list<?php echo $count?>.png" />
	    					<h4><?php the_sub_field( "field_58c7ee70628f5", 4 ); ?></h4>
	    					<?php the_sub_field( "field_58c7ee7b628f6", 4 ); ?>
	    				</div>
	    			</li>
	    		<?php $count++; endwhile; ?>
	    		</ul>
	    		
			</div>
		</div>
	</div>




</session>
<?php include 'footer.php'; ?>
