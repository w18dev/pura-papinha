<?php include 'header.php'; ?>

<session id="banner-dicasnutri">
    <div class="">
        <div class="row">              
            <div class="col-md-12 col-xs-12">
        		<?php $image = get_field_object( "field_58cc486b2cc27", 48 ); ?>
        		<img style="width: 100%; height: 0 auto;" src="<?php echo $image['value']['url']; ?>" />
            </div>
        </div>
    </div>
</session>
<session id="onde-encontrar">
    <div class="container center">          
        <div class="row">          
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id="tituloOndeEncontrar">
                    <img class="center" src="<?php bloginfo('template_directory'); ?>/images/title-onde-encontrar.png" />
                </div>
            </div>
        </div>
    </div>
	<div class="container center">          
    	<div class="row" style="margin-bottom: 30px;">     
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id="form-search">
                    <form id="formPesquisa" action="" method="POST">
                        <input type="text" name="txtPesq" placeholder="Digite seu CEP, e encontre as lojas mais perto de você" >
                        <span class="btn button-search" href="#">
                            <i class="fa fa-search btnSearch"></i>
                        </span>
                    </form>
                </div>
            </div>
            
            <?php 
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                if($_POST['txtPesq']):
                    $query = "s=".$_POST['txtPesq']."&post_type=duvida&post_status=publish";
                    $args = array(
                        'prev_next'          => false,
                        'prev_text'          => '<',
                        'next_text'          => '>',
                        's'                  => $_POST['txtPesq'],
                        'post_status'        => 'publish',
                        'paged'              => $paged,
                        'post_type'          => 'onde_encontrar'
                    );
                else:
                    $args = array(
                        'prev_next'          => false,
                        'prev_text'          => '<',
                        'next_text'          => '>',
                        'post_status'        => 'publish',
                        'paged'              => $paged,
                        'post_type'          => 'onde_encontrar'
                    );
                endif;

                $posts = query_posts($args);
                while(have_posts()):
                    the_post();
                    $postId = $post->ID;
                    $titulo = $post->post_title;
                    $valores = get_fields($postId);
            ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 linhaOndeEncontrar">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 info">
                    <h2><?php echo $titulo; ?></h2>
                    <h3><i class="fa fa-map-marker"></i>Endereço</h3>
                    <div class="textInfo"><?php echo $valores['endereco']; ?></div>
                    <span><i class="fa fa-phone"></i> <?php echo $valores['telefone']; ?> </span>
                    <span><i class="fa fa-whatsapp"></i> <?php echo $valores['whatsapp']; ?> </span>
                    <div class="divCompreAqui">
                        <br/><a href="#" class="botao botao-carrinho btnCompreAqui">COMPRE AQUI</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 infoHorario">
                    <h3><i class="fa fa-clock-o" aria-hidden="true"></i> Horário de Funcionamento</h3>
                    <?php echo $valores['horario_de_funcionamento']; ?> 
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 infoMapa">
                    <?php $tipo = ($valores['tipo_de_loja']=="propria") ? 1 : 2; ?>
                    <?php if($valores['mapa']['lat']!=""): ?> 
                        <div class="map" style="height:217px; width: 100%; color:#FFF;"><?php echo $valores['mapa']['lat']; ?>,<?php echo $valores['mapa']['lng']; ?>,15,<?php echo $tipo; ?></div>
                    <?php endif; ?>
                    <div class='tipoLoja'>
                        <?php echo ($valores['tipo_de_loja']=="propria") ? "<i class='fa fa-home' aria-hidden='true'></i>  Loja Própria" : "<i class='fa fa-map-marker' aria-hidden='true'></i>  Ponto de Venda"; ?>
                    </div>               
                </div>               
                <div class="hidden-lg hidden-md hidden-sm col-xs-12 divCompreAqui2 ">
                    <a href="#" class="botao botao-carrinho btnCompreAqui">COMPRE AQUI</a>
                </div>               
            </div>
            <?php endwhile; ?>   
            
        </div>
    </div>

</session>
<?php include 'footer.php'; ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAL8O-BX1vove6E9PVKB6Gsp1hgmVucx9E&callback=initialize" async defer></script>
<style type="text/css">
    .mapaGoogle{ height: 200px; width: 200px; }
</style>
