<?php include 'header.php'; ?>
<?php 
	if($_POST['txtPesq']):
	    $query = "s=".$_POST['txtPesq']."&post_type=duvida&post_status=publish";
	else:
	    $query = "post_type=duvida&post_status=publish";
	endif;

	$posts = new WP_Query($query);
?>
<session id="banner-dicasnutri">
    <div class="">
        <div class="row">              
            <div class="col-md-12 col-xs-12">
        		<?php $image = get_field_object( "field_59145713eefda", 88 ); ?>
        		<img style="width: 100%; height: 0 auto;" src="<?php echo $image['value']['url']; ?>" />
            </div>
        </div>
    </div>
</session>
<session id="duvidas">
    <div class="container center">          
        <div class="row">          
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id="tituloDuvidas">
                    <img class="center" src="<?php bloginfo('template_directory'); ?>/images/titulo-duvidas.png" />
                </div>
            </div>
        </div>
        <div class="row">          
            <div id="lista-duvidas" class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            	<div id="form-search">
		    		<form id="formPesquisa" action="" method="POST">
		    			<input type="text" name="txtPesq">
		    			<span class="btn button-search" href="#">
	  						<i class="fa fa-search btnSearch"></i>
	  					</span>
		    		</form>
        		</div>
        		<ul>
	        		<?php if (count($posts->posts) > 0) : ?>
		        		<?php foreach ($posts->posts as $kVal => $val) : ?>
			        		<li>
			        			<div class='numero'> <?php echo $kVal+1; ?> </div>
			        			<div class="pergunta"> <?php echo $val->post_title; ?> </div>
			        			<div class="resposta"> <?php the_field( "resposta", $val->ID); ?> </div>
			        		</li>
		  				<?php endforeach; ?>
	  				<?php else: ?>
	  					<li class="error"> Não encontramos nenhuma duvida com: <strong><?php echo $_POST['txtPesq']; ?></strong>
	  				<?php endif; ?>
        		</ul>
        	</div>
            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
        		<h3 class="titulo-sidebar">Fique a vontade para tirar suas dúvidas. Aqui é o lugar!</h3>
        		<hr/>
        		<form action="" method="GET">
	    			<input type="text" name="nome" placeholder="Nome Completo">
	    			<input type="text" name="email" placeholder="E-mail">
	    			<textarea name="pergunta">Pergunta </textarea>
	    			<input type="submit" name="btnEnviar" value="Enviar" >
	    		</form>
        	</div>        	
        </div>
    </div>



</session>
<?php include 'footer.php'; ?>