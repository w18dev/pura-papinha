    <?php include 'header.php'; ?>
 		<section class="row page-servicos--list">
        	<div class="container center">
	            <div class="col-xs-12 col-lg-10 center">
	                <div class="col-xs-12 col-lg-8 container center">
	                	<div class="text-center">
	                		<h2>Atividades exclusivas Mãe Coruja</h2>
	                		
	                	</div>
	                    	<?php 
	                    			the_post();
	                    			$post = get_post();        
	                    			$postId = $post->ID;
	                    			/*echo "<pre>";
	                    			print_r($post);
	                    			echo "</pre>";*/
	                    			$icone = get_post_meta($postId, 'wpcf-icone-servico', true);   		

	                    	?>
			                    <div class="page-servicos-item--text">
			                    	<div class="page-servicos-item--icon">
			                    		<img src="<?php echo $icone; ?>" />
			                    	</div>
			                    	<div class="page-servico-item--box">
				                    	<h3><?php the_title(); ?></h3>
					                    <p>     	
					                    	<?php the_content(); ?>
					                    </p>
					                     <a href="<?php bloginfo('url'); ?>/nossos-servicos"> <i class="fa fa-arrow-left"></i> Voltar </a>
			                    	</div>
			                    </div>
	                </div>
	            </div>
	        </div>
        </section>
    <?php include 'footer.php'; ?>