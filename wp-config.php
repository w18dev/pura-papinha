<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa user o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'w18_pura_papinha');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'frasW5xaBRes');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[5Ou~&mBFh>Onzxb4<K!t]cS:.ZwPJ01e9tBLqEd{j;?CR<~@;dq|nLituSMTFv0');
define('SECURE_AUTH_KEY',  'uuM6)ws7?sbw4|5L#g;0.9|m9ax5Ub<D#$DsYgkSd<|sizZF]iCMER_A@/;$<Nn:');
define('LOGGED_IN_KEY',    '%#3WA0}v~[NxlO{Z^D:vp@1D{#9%Tty]Lpu[|I.;;1gPj5h9=s^yMEdl!?QeeMX4');
define('NONCE_KEY',        'MaM)0^Avc<75N[^o/<<O)ZPJSgvi`|@*cHoqGvz2W&?L4/ TJ66]C+rP@/ dMHR&');
define('AUTH_SALT',        '-*eD^g,Z/TimJp@v.fj_eNp=B-~~*$Sw22f>^%@Pkp0w~Wd2lV<6B;*v+<I7aaJ&');
define('SECURE_AUTH_SALT', '&@T%p_Cezo B$?0{(-972&rdv&?70|rAKXa)]Evmx<Ok2 SB^ygA_jD v,u|1KMR');
define('LOGGED_IN_SALT',   '`2S)_S4@WY^Nnq6<So+G[`hXcEC8E8ByuQI*,V6%M$ LD&$:po+~1U+&4p*HNAq[');
define('NONCE_SALT',       'c/>.#Ps:vo$|<s!v/x?F8---ZxOWZIUHrh)<c}G*|mTgve@wWj4W2j9kO=/T* ,`');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * para cada um um único prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
